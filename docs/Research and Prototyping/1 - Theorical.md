# Theorical aspects of our project

Before starting to explain our project process, we have to explain the theorical ideas that enable us to progress during the project advancement. To obtain more informations on the theorical aspect of our project,  we asked our resource, the  acoustic professor Jean-Louis Migeot and we referred ton Ana's previous acoustic classes from her Uni in Portugal. 

More specifically, we will discuss what sound is and how it is measured, how we hear (and how our hearing is damaged), how a microphone works and, most importantly, **why we chose to work with a dB scale instead of a dBA weighted scale**.

## Sound

What is a dB?
The decibel (dB) is a logarithmic unit used to measure sound level. It´s basically the ration between the power, sound pressure, voltage or intensity and their respective references. 
It's important to precise power and intensity definitions in acousitc domain. Power represents the energy transmitted by the collision of air molecules with a membrane (that can potentially oscillates). The problem is that direct measurement of acoustic power seems pretty diffcult. Fortunately, it's easier mesure to intensity, that is surface normalized power. Intensity itself can be easily obtained by mesuring the pressure at the membrane of the microphone, for example. 

`dB=10 log⁡(I/Io)` , where I=mesured intensity and Io=reference intensity (in W/m**2)
or
`dB=20 log⁡(Pmáx/(Pmáx0))`, where P represents the power (in W)

The sound intensity I is related with the maximum pressure that the propagation way suffers through the relation.

`I=(Pmáx)^2/2ρv`

ρ is the property of the tissues in the auditive channel and v the sound velocity. In case of a microphone, this intensity is the current or the voltage after the membrane (see [How does a microphone work](#how-a-microphone-works)).

Usually, P máx 0 is the reference pressure, and used to be 20 μPa
0 dB measured: It happens when the measured pressure is equal to the reference. Doesn´t mean that the sound intensity is 0.
As we can see: sound level = 20 log (pmeasured/pref) = 20 log 1 = 0 dB
It is also possible to have negative sound levels: - 20 dB would mean a sound with pressure 10 times smaller than the reference pressure, i.e. 2 μPa.

## Human hearing system

Through this image we can see how the human ear is made up, and through the following links we can see how the process of passing sound from a mechanical wave to an electrical signal that is picked up by the brain works.

![](images/ear.jpg)

[How do we hear?](https://www.nidcd.nih.gov/health/how-do-we-hear)

In a nutshell, the sound wave causes the eardrum vibration that pushes the ossicles in a single movement. The stirrup moves like a piston inside the inner Ear, and causes vibration through the perimyph (inner Ear liquid). These vibrations go up and down the spiral part of the inner ear, the cochlea via two seprated canals, then they are transmitted indirectly to hair cells ,themselves connected to nerve endings. The fold of the lashes above the cells against basal membrane provokes neurotransmitter emission. The hair cells at the cochlea apex are more high frequency sensitive, while those at the cochleau bottom are more low frequency sensitive.

The human ear does not respond the same way to all freq. Our ear is more sensitive to sounds between 1 and 7 kHz, but we can hear between 20Hz and 20kHz. That´s why we thought of using an “A weighting filter” where the sound intensity is given in dBA and not dB.

### dBA

A dB(A) measurement has been adjusted to consider the varying sensitivity of the human ear to different frequencies of sound. Therefore, low and very high frequencies are given less weight than on the standard decibel scale. Many regulatory noise limits are specified in terms of dBA, based on the belief that dBA is better correlated with the relative risk of noise-induced hearing loss.

A dBA is a weighted scale for judging loudness that corresponds to the hearing threshold of the human ear. Although dB is commonly used when referring to measuring sound, humans do not hear all frequencies equally. For this reason, sound levels in the low frequency end of the spectrum are reduced as the human ear is less sensitive at low audio frequencies than at high audio frequencies.

Compared with dB, A-weighted measurements underestimate the perceived loudness, annoyance factor, and stress-inducing capability of noises with low frequency components, especially at moderate and high volumes of noise.

A-filter :  
![](images/DBA_filter.jpg)

### The Audiogram

-	The normal functioning of the human ear allows us to be sensitive to frequencies ranging from 20 Hz to 20,000 Hz in adults. This capacity is diminished over the years.
-	Our ear can detect sound differences in the order of 0.1% of the frequency considered.

![](images/diagrama.png)

Diagram legend:

* Patamar - Hearing threshold
* Média - Mean
* Desconforto - Discomfort level
* Dor - Pain level
* Intensidade do som - Sound intensity
* Frequeência - Frequency
* log da internsidade - intensity log


Based on the last diagram, we concluded that **we should use dB instead of dBA**. We can see that although the hearing floor (*patamar*) is correlated to the A-filter, the pain and damage limits are relatively constant relative to frequency. This is because as soon as a noise is perceivable, some hair cells in our ear will vibrate and react to that sound (see the [How do we hear](#human-hearing-system) section above). If the intensity (in dB) is too high and the sound frequency is audible (20-20 000 Hz), the hair cells which perceive that sound will be damaged. Therefore, we don't need to use any filter to give more importance to some frequencies and reduce others.


## How a microphone works
 To fully understand our approach, it's useful to know how a microphone works. 
 In simple terms, the goal of a microphone is to convert an acoustic signal (sound pressure) into an electric signal.
 
### Acoustic waves  
 But what is the nature of acoustic signal. As we discussed before, it's a pressure difference that propagating in the air. That is, when a sound is emitted from somewhere, it will propoagating through space like a wave signal, creating alternatively high pressure zone, where air is compressed and low pressure, where air is rare. This wave signal will spread in the air. 

### Different types of microphone 
We can distinguish two types of microphone in the way they convert acoustic signal into electric signal : electrodynamic and electrostatic microphones.

#### Electrodynamic microphone
In a nutshell, electrodynamic microphones include a membrane linked to a magnet, itself surrounded by a metal coil. When sound comes to the microphone, the acoustic wave causes the movement of the membrane and consequently of the magnet. The variating magnetic field generates inside the coil a induced current. Thus each movement of the membrane can be translated into a voltage, mesured at the two extremities of the coil. Then the low electric signal is amplified by a preamplifier.  


![](images/microdyn.jpg)   


#### Electrostatic microphone 
The electrostatic microphone also works with sound sensible mobile membrane, but this time it is connected to an electrode of a capacitor. The other electrode of the capacitor is fixed. The movement of the membrane due to an acoustic wave causes a distance variation that results in a capacity variation and therfore that generates a voltage.

![](./images/microstat.png)  

Our microphone module, the INMP 441, is an electrostatic microphone. 






