# Earplugs

First and foremost, we will explain why we discarded the idea of creating earplugs that filter out the construction noise frequencies while letting human voice still be audible. As you can see on the graphs below (the left one shows the typical frequencies during a conversation, the right one was recorded on a construction site), the frequencies of construction cover a very big spectrum including, most notably, human speech. It is thus impossible to cancel out work noises without cancelling speech as well. Both graphs were recorded on iOS App PhyPhox.  

![](./images/graphs.png)

The first idea we had was to make earplugs ourselves that would be low cost and that could adapt to every ear. But after some researches we saw that this was not an option for our project. Indeed, low cost kits                                                    already exist to make foam earplugs at home.  
The second option was to make them in silicone but this only possible for custom made earplugs and that was not the goal of our project.  
The third option was to 3D print earplugs using a differente type of filament. We would have used TPU instead of PLA because this filament is flexible but still not enough to make an earplug that would fit every ear. They would still need to be custom made like the silicones ones.  
Our last option and the one we took was to use regular earplugs and thus not create earplugs in addition to our detector. 


We needed to decide the earplugs which we would take. After some research we saw that the SNR values changes between the different types of earplugs. The SNR value is the Signal to Noise Ratio and tell us the protection given by the earplugs in decibel. This values is an average of the number of decibel the earplugs will attenuate. We still need to be careful with these values because they are averages. The values will not be the same at different frequencies. That why we can use the HML values, they are the values at High, Medium or Low frequency. 
![](./images/sachet_earplugs.jpg)  
 In a construction site, the noises will cover a large spectrum of frequencies, therefore **we will not take into account the HML values**.  
 ![](./images/tableau%20valeurs%20sonores%20sur%20chantier.png)


The last constraints is that the foam earplugs are single use. Solution like the [Alpines](https://www.alpinehearingprotection.com/) earplugs existe but the SNR value is a little bit lower and they are more expensive. **The choice between the single use or reusable earplugs is up to the person using the detector**, we just provide an universal emplacement for any type of earplugs with our box.  
![](./images/earplugs%20choix%20final.jpg)
