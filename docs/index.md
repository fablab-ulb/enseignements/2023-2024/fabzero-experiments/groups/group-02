# Group and project presentation

**How to help construction workers with the high level of noise that they encounter in their working environment.**

## About our team
Our team is composed of :  

- [Ana Fernandes](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/anapatricia.fernandes), 1st year of Master in Biomedical Engineering and currently doing an Erasmus exchange in Brussels   
- [Jean Boussier](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/jean.boussier), 3rd year of Bachelor in Bioengineering at ULB   
- [Diane Bilgischer](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/diane.bilgischer), 3rd year of Bachelor in Bioengineering at ULB    
- [Lukas Schieble](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/lukas.schieble), 3rd year of Bachelor in Bioengineering at ULB   

Here we are :
![](./images/grupo.png)  
As you can see, 3 of us are studying Bioengineering while Ana is in the biomedical sector. Polydisciplinarity is therefore not our biggest strength but being surrounded by people with the same interests and fields of studies can be a huge benefit when it comes to decision making. Our team is also pretty international, with Lukas being from Germany, Jean from France, Ana from Portugal and Diane from Belgium with Swiss ancestry. This was a major advantage during the project because our points of view and visions on some topics were very diverse due to our different backgrounds which is always good for a more reflected and smart decision making.  
As you'll see while you're reading our documentation, we chose to write it in English, however, some figures and schemes are in French or Portuguese due to our respective mother tongues.  
## Our Project abstract
Noise pollution is a major health issue in our World. We wanted to focus our efforts to help a group of people which is particularly exposed to this problem: construction workers. The statistics are alarming. According to the [US government](https://www.cdc.gov/niosh/topics/noise/surveillance/construction.html#:~:text=Hearing%20Loss%20and%20Tinnitus&text=About%2025%25%20of%20noise%2Dexposed,have%20a%20material%20hearing%20impairment.&text=Hearing%20impairment%20is%20hearing%20loss,hearing%20impairment%20in%20both%20ears.), more than 50% of workers have been exposed to hazardous noise and 52% even report not wearing hearing protection. Those factors cause health issues among this profession, namely 25% of workers have hearing issues impacting their daily lives, with over 15% facing severe issues like tinnitus among others.  

We thought that one major problem of noise exposure is that it is "invisible", meaning that you cannot really notice when a sound is above the threshold where it could affect your health. Mobile phones are a solution with apps like PhyFox to measure sound intensity, however, some issues remain. First, a lot of construction workers, especially in the industrial domain, are not allowed to carry their mobile phones with them due to copyright and confidentiality issues. Also, those apps first need to be calibrated using a real sound sensor, which can be quite expensive. Finally, a classical sound meter just displays the numerical values which can be confusing. In most cases, they don't really show us explicitly when the healthy limit is crossed. 

The goal of our project is thus to make the invisible visible, in this case showing when the sound level exceeds the healthy limit by making a LED light up. This LED lights up either when the short-term limit (around 115 dB) is crossed, or when the long term threshold (around 85 dB) is exceeded for 2 hrs in total.  
![](./images/level.jpg)  

We created a small portative device that can be carried around the worker's belt and that show's explicitly when to wear ear protection. The protections are included inside the device and consist of standard foam earplugs. 


## Our final Project

![](images/final_image.jpg)
