# Process

Here, we will describe our process from the formation of the groups until the final presentation in January. 
## Group formation

You can watch our individual docs (Module 5; [Ana](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/anapatricia.fernandes), [Diane](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/diane.bilgischer), [Lukas](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/lukas.schieble) and [Jean](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/students/jean.boussier)) to get more detailed information on our individual thoughts. Here we will only focus on the group's perspective.  

|Name|Object|Problematic|  
|--------------------|:--------------------:|--------------------|  
|Ana|Empty Pencil Container|Plastic Waste, consumerism|  
|Jean|Adjustable Wrench|Building railways on your own|  
|Lukas|1€ Coin|Money-dominated society|  
|Diane|Architecture Book|Lack of housing|  


So, everyone had to bring an object to class and go talk to other people that brought similar objects or just an object representing an interesting problematic. Pretty naturally, little groups of 3-4 people were forming. The 4 of us were explaining their problematics to each other when suddenly Sophie, the pedagogical adviser of the FabLab, came towards us and shouted something like : "*Ey, you, sit down, you are a group now*".  
After that, we had to write down a list of problemetics on an A3 sheet, exchange with other groups and finally vote for the problematics that interested us the most. Our most popular problematics were :  

1. How to raise awareness on electricity consumption  
2. How to analyze your blood at home  
3. How to improve compost quality  
4. How to purify water  

As you might have noticed, those aren't problematics but already possible solutions. This very solution-orientated mindset of us would become our biggest enemy during the first weeks of the project, as you will see throughout this documentation (in [this section](#brainstorming) mainly).  

## Problem brainstorming and Group Dynamics 

### Group Dynamics

First of all, we started by listing what each of us thought were the key elements for creating a good group dynamic. From there we put together the most important ones, resulting in the following list.

1. Good comunication with mutual respect. Not interupting anyone while they´re talking.  
2. Good splitting of the tasks.  
3. Well organized to-do lists.  
4. Respecting the milestones and making realistic timetables.  
5. Don´t worry, be happy. :) (the most important one)  

Then we decided when we would meet to carry out the work. Based on everyone's weekly schedules, we decided that we would keep the class timetable for our meetings. So Mondays or Tuesdays, from 12 pm to 2 pm.
We also decided that we would use Discord and WhatsApp to work remotely.

Finally, we delegated tasks. This part is very important so that we can all realize what our role in the group is, even though we all contribute equally, this type of division is important to ensure the good functionality of the group and avoid an anarchic system.

In the image below, you can see our draft made in class.

![](images/group_dynamic.jpg)


### Brainstorming

Thinking about our problem and working on it was the biggest challenge we faced, because we started thinking about a solution beforehand and not the initial problem.
We had several initial ideas and travelled a long way to arrive at the final problem.

#### 1st Try

As our main theme was health-related, the first problem we tackled was the underdevelopment of health systems in third world countries. From this first thought came the following problem tree.

 ![](images/1st_prob.jpg)

 We quickly realised that this problem was too broad and we needed something more concrete to work with. After talking with Denis, we decided to restart our brainstorming process from the beginning. We tried to avoid thinking about the solutions we'd like to develop and to have a more problem-orientated mindset, which revealed to be really hard. However, we still came up with 2 problematics:  
 
 1. High stress levels among students.
 2. Overpriced healthcare in underdeveloped countries.

Here you can see the problem trees we built on **Miro** ([link](https://miro.com/app/board/uXjVNRv7dTo=/) to our project if you want to be able to read the little details):  
![](./images/problem.png)

With regard to the first sub-theme, we can say that it fits in perfectly with the notion of the invisible problem, which was one of the parameters addressed in this module, however, the solution also involves something invisible and impossible to do in the environment in which we find ourselves, or at least we couln't think of anything really useful. So we decided to continue with the problem of high healthcare prices in developed countries.

Taking this problem, we tried to dive into a more concrete branch of health, so why not the problems related to high noise levels in countries like India? Based on Jean's experience of travelling, he realised that this problem really does affect the lives of the country's habitants. It can also be turned into a "making invisible visible" type of problem, for example by helping deaf people recovering their senses.  

But, as always, there was an impediment to continuing with this problem: the fact that we couldn't test our solution, because effectively nobody was planning to go to India any time soon. So let's think about this problem that also exists in Brussels. More specifically in construction.
This led to the following problem-consequence tree.

![](images/final_tree.jpg)

We've finally found a problem that we can work with - **The high level of noise that construction workers encounter in their working environment.**


## Prototyping a Solution and Hackathon 

We started by thinking of a device that could filter the sound and allow human voices to pass through while eliminating the sound caused by machines, for example. This would allow workers to communicate well with each other without being affected by external noise. We created a first model, which consisted of a kind of headphones. 

![](./images/prototype.jpg)  

We also had to think about a silly prototype. Jean came up with a very funny idea, which was to design earrings with a LED lamp attached to it. This LED lights up in red when the noise intensity reaches a certain level, 80 dB for example, warning the user of a high noise level that could damage his ears. We were thinking of giving it to party people as a cool gadget. Here you can see our prototype.  

![](./images/IMG_1404.jpg)

We also had two other ideas that we didnt have time to prototype but which we wrote down and kept in mind, which were:  

1. Making a cheap hearing aid that is accessible to everyone.
2. Earplugs with sound detector that emits an alert when dB exceed the healthy limit.

We had to show our prototypes to the other groups and to some external people. We also needed to find some resource people in order to help us with the further steps of our projects.  
Most part of the people liked our problematic and our solution ideas, however, some people reminded us that noise-cancelling headphones with implemented frequency filter already exist and aren't that expensive. Some doubts started to emerge and it became clear that we might need a plan B and maybe even a plan C to conduct research about during the following weeks.  

About the mentors, we started talking to Martin, an assistant at the FabLab who is currently studying Electromechanical Engineering. He told us that he followed a class on acoustics last year taught by Prof Jean-Louis Migeot, who is an expert in modeling of acoustical phenomena. Martin gave us his coordinates and the e-mail of the Prof. Some other people in our respective friend circles also came to our mind as potential resource people. Here's the final list of our mentors:  

* **Martin**, the FabLab assistent  
* **Jean-Louis Migeot**, Martin's professor for acoustics  
* **Sam**, Jean's friend, who is also a sound engineer  
* **Axel**, the electronics expert at FabLab  
* **Rafal**, a friend of Lukas who is a construction worker  

Finally, we needed to split the tasks for the following meeting. Everyone had to conduct research on one of the ideas we had. Jean and Lukas chose to investigate on the hearing aid while Diane and Ana searched on the noise cancelling headphones 

## Try, Fail, Repeat (our weekly Meetings)

### Finding our solution

Unfortunately, while doing the research, both ideas turned out to be practically unfeasible, at least at our scale. Both involve too much Materials Engineering and Electronics, which are fields where none of us is competent. This was another failure for our project... For example, here you can see an image of a hearing aid built with Arduino and electronics (credits to [ojoshi](https://www.instructables.com/member/ojoshi/)), the size of the device is impressive.  
![](./images/crazy-project.webp)  
However, during the meeting, we thought of something way less ambitious and thus more feasible. Based on the idea from last week's hackathon (the earplugs with sound detector), we imagined a portable case containing a noise level detector and earplugs. When the detector shows a high level on the construction site (for example while using a chipping hammer), the worker simply wears the earplugs and is protected.  

### Project planning and division of tasks 

After finding a solution we start to outline it more concretely, starting also by dividing tasks.  

Below follows our work scheme.

![](images/solution.jpg)  

Because Jean and Lukas did the microcontroller formation and Ana and Diane the lasercutter/3D repair one, those tasks seemed like the smartest choice to us. We kept those tasks until the end of the project, except for Electronics and Arduino, which we spiltted into Electronics (Lukas) and research on sound and acoustics (Jean). In the other pages of this documentation, you will see our thoughts, fails and progression in each one of the tasks ([Electronics](https://group-02-fablab-ulb-enseignements-2023-2024-fabz-4729e3c9136aa4.gitlab.io/Process/Eletronics/), [Sound/Theorical research](https://group-02-fablab-ulb-enseignements-2023-2024-fabz-4729e3c9136aa4.gitlab.io/Process/Theorical/), [3D design](https://group-02-fablab-ulb-enseignements-2023-2024-fabz-4729e3c9136aa4.gitlab.io/Process/3D/) and [Earplugs](https://group-02-fablab-ulb-enseignements-2023-2024-fabz-4729e3c9136aa4.gitlab.io/Process/Earplugs/)).


